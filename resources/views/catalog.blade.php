@extends('layouts.app')
@section('content')
<h1 class="text-center py-5">CATALOG</h1>
@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif
<div class="container">
	<div class="row">
		@foreach($items as $indiv_item)
		<div class="col-lg-4 my-2 p-3">
			<img class="card-img-top" height="280px" src="{{$indiv_item->imgPath}}">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{$indiv_item->name}}</h4>
					<p class="card-text">Price: {{$indiv_item->price}}</p>
					<p class="card-text">Item Description: {{$indiv_item->description}}</p>
					<p class="card-text">Category: {{$indiv_item->category->name}}</p>
				</div>
				<div class="card-footer d-flex align-items-center justify-content-center">
					<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit">Delete</button>
					</form>
					<a href="/edititem/{{$indiv_item->id}}" class="btn btn-success w-25">Edit</a>
				</div>
				<div class="card-footer">
					<form method="POST" action="/addtocart/{{$indiv_item->id}}">
						@csrf
						<input type="number" name="quantity" class="form-control" value="1">
						<button class="btn btn-primary btn-block" type="submit">Add to Cart</button>
					</form>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection
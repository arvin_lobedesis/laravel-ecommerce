<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// show catalog
Route::get('/catalog','ItemController@index');

// show add item category option
Route::get('/additem','ItemController@create');

// to save
Route::post('/additem','ItemController@store');

// delete item
Route::delete('/deleteitem/{id}','ItemController@destroy');

// edit item
Route::get('/edititem/{id}','ItemController@edit');

// update item
Route::patch('/edititem/{id}','ItemController@update');

// cart crud
Route::post('/addtocart/{id}','ItemController@addToCart');
Route::get('/showcart','ItemController@showCart');
